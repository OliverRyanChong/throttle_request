# throttle_request

Script to test request throttling between servers.
It also has a request retry logic if the initial server request times out.

---
Start the BRM mock/fake server (running on port 3000):
```sh
node throttle_test_brm_server.js
```

Start the BSS MW mock/fake server (running on port 3001):
```sh
node throttle_test_bssmw_server.js
```

To test, make a request to the mock BSS MW server which will then route the request to the mock BRM server.
To simulate making requests to the BSS MW mock server, enter this in your command line terminal:
```bash
wrk -t1 -c12 -d10s -s ./testPost.lua http://localhost:3001/test/
```