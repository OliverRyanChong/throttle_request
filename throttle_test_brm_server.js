const express = require('express');
const app = express();
const port = 3000;

const generateRandomNum = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

app.use(express.json());       // to support JSON-encoded bodies
app.use(express.urlencoded()); // to support URL-encoded bodies

app.post('/test', (req, res) => {
    console.log('request body:', req.body); 

    // randomize the success response
    let randNum = generateRandomNum(1, 10);
    // about 70% chance for success
    if (randNum > 3) {
        // randomize the delay for the server response
        let randNumForDelayMs = generateRandomNum(1, 100);
        setTimeout(() => {
            // if the delay for the response is more than 70ms
            if (randNumForDelayMs > 70) {
                // request timeout
                console.log('response timeout for request number ' + req.body.reqId + ' with a delay of ' + randNumForDelayMs + ' ms');
                res.status(408).json({ reqId: req.body.reqId, msg: 'Server took too long to respond' });
            } else {
                console.log('response success for request number ' + req.body.reqId + ' with a delay of ' + randNumForDelayMs + ' ms');
                res.send({ reqId: req.body.reqId, msg: 'Hello World from the server' });
            }
        }, 
        randNumForDelayMs);
    } else {
        console.error('response fail for request number ' + req.body.reqId);
        res.status(500).json({ reqId: req.body.reqId, msg: 'Some random error happened in the server' });
    }
});

app.listen(port, () => console.log(`Server listening on port ${port}!`));