// https://www.npmjs.com/package/bottleneck
const Bottleneck = require('bottleneck');

// https://github.com/request/request
const request = require('request');

const express = require('express');
const app = express();
const port = 3001;


app.use(express.json());       // to support JSON-encoded bodies
app.use(express.urlencoded()); // to support URL-encoded bodies

app.post('/test', (req, res) => {
    console.log('request body:', req.body); 
    makeRequest(req, res);
});

app.listen(port, () => console.log(`Server listening on port ${port}!`));


//////////////////////////////////////////////////////////////////////////////////////////////////////////

// the suppported request methods
const REQUEST_METHODS = {
    GET: 'GET',
    POST: 'POST'
};

// the header content types in the HTTP request
const HEADER_CONTENT_TYPES = {
    JSON: 'application/json',
    XML: 'application/xml',
    FORM: 'application/x-www-form-urlencoded'
};

const DEFAULT_ENCODING = 'utf-8';

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// the constants for throttling the request
const THROTTLE_REQ_CONST = {
    BSSMW_INST_NUM: 1, // manually count the number of running BSS MW instances in all server machines
    REQ_PER_SEC_LIMIT: 10, // the BRM system can only support a max of 250 requests per second
    MAX_CONCURRENT_PROCESS: 5,
    MAX_MS_FOR_JOB_TO_COMPLETE: 10,
    REQ_INTERVAL_MS: 1000
};

// number of requests allowed per second (distributed by the number of BSS MW instances running)
let numOfReqPerSecAllowed = Math.floor(THROTTLE_REQ_CONST.REQ_PER_SEC_LIMIT / THROTTLE_REQ_CONST.BSSMW_INST_NUM);


// the request rate limiter
const limiter = new Bottleneck({
    // the number of max concurrent processes allowed within the min time (eg. 2 max concurrent processes allowed in 100 ms) 
    maxConcurrent: THROTTLE_REQ_CONST.MAX_CONCURRENT_PROCESS,
    // How long to wait after launching a job before launching another one.
    //minTime: Math.floor(THROTTLE_REQ_CONST.REQ_INTERVAL_MS / numOfReqPerSecAllowed), 
    minTime: 10,

    // in-memory queue limit
    highWater: 1, //How long can the queue be? When the queue length exceeds that value, the selected strategy is executed to shed the load.
    strategy: Bottleneck.strategy.OVERFLOW, // When adding a new job to a limiter, if the queue length reaches highWater, do not add the new job. This strategy totally ignores priority levels.

    // rate limit the number of requests per second
    reservoir: numOfReqPerSecAllowed, // the number of allowed requests per second
    reservoirRefreshAmount: numOfReqPerSecAllowed,
    reservoirRefreshInterval: THROTTLE_REQ_CONST.REQ_INTERVAL_MS // the reservoir will be refreshed with reservoirRefreshAmount after every time interval
});

//////////////////////////////////////////////////////////////////////////////////////////////////////////

const RETRY_MAX_COUNT = 2;
const RETRY_START_SEC = 1;

/**
 * Send the throttled request
 * @param {string} reqUrl
 * @param {Object} reqParams 
 * @param {Function} callback
 * @returns {Object}
 */
const sendThrottledReq = async (reqUrl, reqParams, retryNum, callback) => {
    //console.log('sendThrottledReq --- reqUrl:', reqUrl);
    //console.log('sendThrottledReq --- reqParams:', reqParams);

    let result = null;
    try {
        // make the throttled request
        // if the request expires, the library auto-retries
        result = await limiter.schedule({ expiration: THROTTLE_REQ_CONST.MAX_MS_FOR_JOB_TO_COMPLETE }, () => {

            let headerContentType = HEADER_CONTENT_TYPES.JSON;
            let requestMethod = REQUEST_METHODS.POST;
        
            // the request header parameters
            const reqHeader = {};
            reqHeader['Accept-Charset'] = DEFAULT_ENCODING;
            if (headerContentType && requestMethod == REQUEST_METHODS.POST) {
                reqHeader['Accept'] = headerContentType;
            }
        
            // the request options
            const reqOptions = {
                url: reqUrl,
                method: requestMethod,
                headers: reqHeader,
                //body: A Buffer, String, or Stream object (can be an object if json option is set to true)
                json: (requestMethod == REQUEST_METHODS.POST) ? true : false,
                body: reqParams
            };

            //console.log('sendThrottledReq --- reqOptions:', reqOptions);
        
            // make the HTTP request
            request(reqOptions, (error, response, body) => {
                let resData = null;
                if ( response ) {
                    resData = response.body;
                }
                // Internal Server Error (500)
                // Request Timeout (408)
                if (response && response.statusCode == 408) {
                    console.log(`Server timeout for request id ${reqParams.reqId}...`);
                    // manually retry the server call (in the event of server request timeout)
                    if (retryNum < RETRY_MAX_COUNT) {
                        ++retryNum;
                        let retryDelay = (RETRY_START_SEC * 1000) * retryNum;
                        console.log(`Attempting to retry request id ${reqParams.reqId} with retry attempt number: ${retryNum}...`);
                        setTimeout(async () => {
                            // recursive call
                            await sendThrottledReq(reqUrl, reqParams, retryNum, callback);
                        }, retryDelay);
                    } else {
                        console.log(`Max retries reached for request id ${reqParams.reqId}!`);
                        callback(resData, { reqId: reqParams.reqId });
                    }
                } else {
                    callback(error, resData);
                }
            });
        });

        //console.log('sendThrottledReq --- result:', result);

    } catch (err) {
        if (err instanceof Bottleneck.BottleneckError) {
            console.error(`sendThrottledReq --- BottleneckError: ${err.message} for request id ${reqParams.reqId}`);
            callback(new Error(`Request id ${reqParams.reqId} rejected because there are currently too many requests`), { reqId: reqParams.reqId });
        } else {
            console.error(`sendThrottledReq --- error: ${err.message} for request id ${reqParams.reqId}`);
            callback(err, { reqId: reqParams.reqId });
        }
        
    } finally {
        return result;
    }
};

limiter.once('error', (err) => {
    console.error('sendThrottledReq --- event error', err);
});

limiter.once('dropped', (dropped) => {
    console.warn('sendThrottledReq --- event dropped', dropped);
});


//////////////////////////////////////////////////////////////////////////////////////////////////////////

let reqNum = 0;

const makeRequest = async (req, res) => {
    const NS_PER_SEC = 1e9;
    const MS_PER_NS = 1e-6
    const time = process.hrtime();

    try {
        ++reqNum;
        let result = await sendThrottledReq('http://localhost:3000/test', { reqId: reqNum }, 0, (err, data) => {
            console.log('sendThrottledReq callback ===== reqId:', data.reqId);
            console.error('sendThrottledReq callback err:', err);
            console.log('sendThrottledReq callback data:', data.msg);

            let diff = process.hrtime(time);
            console.log(`After request ${ data.reqId } the elapsed time is ${ (diff[0] * NS_PER_SEC + diff[1]) * MS_PER_NS } milliseconds`);

            if (err) {
                res.status(500).json({ err });
            } else {
                res.status(200).json({ data });
            }
        });
        //console.log('request result:', result);
    } catch (err) {
        res.status(500).json({ err });
    }

}
